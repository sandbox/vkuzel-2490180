<?php

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_conversant_default_rules_configuration() {
  
  $rule = rules_reaction_rule();
  $rule->label = 'Add Conversant measure dot to checkout completion';
  $rule->active = TRUE;
  $rule->event('commerce_checkout_complete')
    ->action('commerce_conversant_add_measure_dot', array(
      'commerce_order:select' => 'commerce_order',
    )
  );
  $configs['commerce_conversant_rule_dot'] = $rule;

  return $configs;
}
