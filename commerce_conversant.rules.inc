<?php

/**
 * Implements hook_rules_action_info().
 */
function commerce_conversant_rules_action_info() {
  return array(
    // Add rules action which sends the analytics code
    'commerce_conversant_add_measure_dot' => array(
      'label' => t('Show Conversant measure dot'),
      'group' => t('Conversant'),
      'parameter' => array(
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order in checkout'),
        ),
      ),
      'callback' => array(
        'execute' => 'commerce_conversant_add_measure_dot'
      ),
    ),
  );
}
