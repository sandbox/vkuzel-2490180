<?php

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function commerce_conversant_admin_settings_form($form_state) {  
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );

  $form['account']['conversant_container_tag_id'] = array(
    '#title' => 'Container tag ID',
    '#type' => 'textfield',
    '#default_value' => variable_get('conversant_container_tag_id', ''),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('The unique identifier for the conversion tag.'),
  );

  $form['account']['conversant_cid'] = array(
    '#title' => 'CID',
    '#type' => 'textfield',
    '#default_value' => variable_get('conversant_cid', ''),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('Your Enterprise ID. The Enterprise ID is an ID that enables a single integration across multiple CJ products.'),
  );

  $form['account']['conversant_type'] = array(
    '#title' => 'Type',
    '#type' => 'textfield',
    '#default_value' => variable_get('conversant_type', ''),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('Parameter that displays the Action ID, a CJ-assigned number that identifies an action that has occurred.'),
  );

  $form['account']['conversant_only_products'] = array(
    '#title' => 'Track only products',
    '#type' => 'checkbox',
    '#default_value' => variable_get('conversant_only_products', ''),
    '#description' => t('If checked only products will be tracked. Shipping will be excluded from tracking.'),
  );

  return system_settings_form($form);
}
